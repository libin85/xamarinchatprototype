﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApplication2
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _action;
        private readonly Action<object> _parameterisedAction;
        private readonly Func<bool> _canExecute;

        public DelegateCommand(Action action, Func<bool> canExecute = null)
        {
            _action = action;
            _parameterisedAction = null;
            _canExecute = canExecute;

            if (canExecute == null)
                _canExecute = () => true;


        }

        public DelegateCommand(Action<object> parameterisedAction, Func<bool> canExecute = null)
        {
            _parameterisedAction = parameterisedAction;
            _action = null;
            _canExecute = canExecute;

            if (canExecute == null)
                _canExecute = () => true;

        }

        public void Execute()
        {
            Execute(null);
        }

        public void Execute(object parameter)
        {
            if (_parameterisedAction != null)
            {
                _parameterisedAction(parameter);
                return;
            }

            _action();
        }

        public bool CanExecute()
        {
            return CanExecute(null);
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute();
        }

        public void SignalCanExecuteChanged()
        {
            var e = CanExecuteChanged;
            if (e != null)
                e(this, EventArgs.Empty);
        }

        public event EventHandler CanExecuteChanged;
    }
}
