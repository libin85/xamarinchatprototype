﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfApplication2.Server;
using WpfApplication2.Annotations;

namespace WpfApplication2
{

    public class ChatPageViewModel: INotifyPropertyChanged
    {
        private readonly IChatService _chatService;
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<ChatMessage> Messages { get; set; }
        private string _inputText;

        public ChatPageViewModel(): this(new DummyChatService())
        {

        }

        public ChatPageViewModel(IChatService chatService)
        {
            _chatService = chatService;
            Messages = new ObservableCollection<ChatMessage>(_chatService.AllMessages);
            SendCommand = new DelegateCommand(() =>
            {
                _chatService.Send(InputText);
                InputText = null;
            }, () => !string.IsNullOrWhiteSpace(InputText));

            chatService.NewMessage += (sender, message) => Messages.Add(message);
        }

        public string InputText
        {
            get { return _inputText; }
            set 
            {
                _inputText = value; 
                OnPropertyChanged();
                SendCommand.SignalCanExecuteChanged();
            }
        }

        public DelegateCommand SendCommand { get; set; }

		[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }


}
