﻿using System;
using Xamarin.Forms;

namespace WpfApplication2.Server
{
    public class DummyChatService: IChatService
    {
        public DummyChatService()
        {
            AllMessages = new[]
                {
                    new ChatMessage {Message = "Hello Eeveryone!", Sender = "Tom"},
                    new ChatMessage {Message = "Whats Happening", Sender = "Lisa"}
                };
            int i=1;
			Device.StartTimer (new TimeSpan (0, 0, 4),  () => {
				if (NewMessage != null)
				{
					NewMessage(this,new ChatMessage{Message = "Yo "+ i++, Sender = "Dummy Server"});
				}
				return true; 
			});
            
        }
        public ChatMessage[] AllMessages { get; private set; }
        public void Send(string message)
        {
            if (NewMessage != null)
            {
                NewMessage(this, new ChatMessage { Message = message, Sender = "You" });
            }       
        }

        public event EventHandler<ChatMessage> NewMessage;

       
    }
}
