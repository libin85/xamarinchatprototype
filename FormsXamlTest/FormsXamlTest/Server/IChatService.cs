﻿using System;

namespace WpfApplication2
{
    public interface IChatService
    {
        ChatMessage[] AllMessages { get; }
        void Send(string message);
        event EventHandler<ChatMessage> NewMessage;
    }
    public class ChatMessage
    {
        public ChatMessage()
        {
            When = DateTime.Now;
        }
        public string Message { get; set; }
        public DateTime When { get; set; }
        public string Sender { get; set; }
    }
}