﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using WpfApplication2;

namespace FormsXamlTest
{
	public partial class testLabelPage : ContentPage
	{
		public testLabelPage ()
		{
			InitializeComponent ();
			BindingContext = new ChatPageViewModel ();
		}
	}
}

